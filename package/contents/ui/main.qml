/*
 * Plasma XR Applet
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
 * SPDX-License-Identifier: MIT
 */


import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.components 2.0 as PlasmaComponents
import QtQuick.Dialogs 1.1



Item {
    id:applet
    Plasmoid.icon: plasmoid.file("images", "../images/vr-symbolic.svg")
    Plasmoid.status:PlasmaCore.Types.PassiveStatus
    Layout.minimumWidth: units.gridUnit * 8
    Layout.minimumHeight: units.gridUnit * 3
    
    MessageDialog {
        id: messageDialog
        title: "Error calling XR desktop dbus interface"
        // default text that should be replaced before showing the dialog
        text: "An error occured"
        onAccepted: {
            enableButton.skip_processing = true
            enableButton.togglestatus = false
            //console.log("And of course you could only agree.")
        }
        Component.onCompleted: visible = false
    }
    
    
    Item {
        id: statusView
        visible: false
        anchors.fill: parent
        
        PlasmaComponents.BusyIndicator {
            id: busyindicator
            visible: !enableButton.visible
            
            
            
            Timer {
                property bool queryfinish:false
                id: busyindicatortimer;
                interval: 1000;
                onTriggered: {
                    if(queryfinish){
                        enableButton.visible = true
                    }
                    queryfinish = false
                }
            }
            
            
            
            anchors {
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
            }
            
            PlasmaExtras.Heading {
                id: xrdesktopstatusheading
                level: 3
                visible: applet.height > (units.gridUnit * 6)
                opacity: 0.6
                text: enableButton.togglestatus ? i18n("Starting...") : i18n("Stopping...")
                
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    bottom: busyindicator.top
                    bottomMargin: units.smallSpacing
                }
            }
        }
        
        
        
        
        PlasmaComponents.Button {
            id: enableButton
            text: togglestatus ? i18n("Disable xrdesktop") : i18n("Enable xrdesktop")
            checked:togglestatus
            iconSource: Qt.resolvedUrl("../images/vr-symbolic.svg")
            // after an error, when setting the button to off, don't trigger the error message again
            property bool skip_processing: false
            property bool togglestatus: false
            
            onClicked: {
                togglestatus = !togglestatus
                toggleAction(togglestatus)
            }
            
            anchors {
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
            }
        }
    }
    
    
    PlasmaExtras.Heading {
        id: headingNoHmd
        width: parent.width
        opacity: 0.6
        level:3
        visible: true
    }
    
    function toggleAction(checked) {
        console.info("xrdesktop enabled: " + checked)
        enableButton.visible = false;
        //reset timer
        busyindicatortimer.running = false;
        busyindicatortimer.running = true;
        execToggle.connectSource('qdbus org.kde.KWin /XR org.kde.kwin.XR.active ' + checked)
    }
    
    
    
    
    
    PlasmaCore.DataSource {
        id: execToggle
        engine: "executable"
        connectedSources: []
        onNewData: {
            //console.info("Toggle reply: " + data.stdout + "; " + data.stderr)
            if (data.stderr !== "") {
                
                // the button is being set to false as a result of an error, don't do anything'
                if (enableButton.skip_processing) {
                    enableButton.skip_processing = false
                    disconnectSource(sourceName)
                    return
                }
                
                messageDialog.text = "Error toggling xrdesktop mode in KWin:\n"
                messageDialog.text += data.stderr + "\n"
                if (data.stderr.toString().match("^Cannot find"))  {
                    messageDialog.text += "Usually this means the xrdesktop plugin is not enabled in KWin or not working."
                }
                messageDialog.visible = true
            }
            if (data.stdout !== "") {
                // TODO: handle error
            }
            disconnectSource(sourceName)
        }
    }
    
    
    
    PlasmaCore.DataSource {
        id: execQuery
        engine: "executable"
        connectedSources: []
        onNewData: {
            //console.info("Timer query reply: " + data.stdout + "; " + data.stderr)
            if (data.stderr == "") {
                var val = false
                if(data.stdout.toString().match("^true")){
                    val = true
                }
                enableButton.togglestatus = val
                if(!busyindicatortimer.running){
                    enableButton.visible = true
                }else{
                    busyindicatortimer.queryfinish = true;
                }
                
            }
            disconnectSource(sourceName)
        }
    }
    
    PlasmaCore.DataSource {
        id: execHmdCheck
        engine: "executable"
        connectedSources: []
        onNewData: {
            if (data.stderr == "") {
                var hmdconnected = false
                if(data.stdout.toString().match("^true")){
                    hmdconnected = true
                }
                //console.info("hmd found:"+hmdconnected);
                if(hmdconnected==false){
                    plasmoid.status = PlasmaCore.Types.PassiveStatus
                    if (data.stdout.toString().match("No such object path"))  {
                        headingNoHmd.text = applet.height > (units.gridUnit * 6) ? i18n("The xrdesktop plugin is not enabled in KWin or not working.") : i18n("The plugin is not enabled.")
                    }else{
                        headingNoHmd.text = i18n("No HMD found")
                    }
                }else{
                    plasmoid.status = PlasmaCore.Types.ActiveStatus
                }
                statusView.visible = hmdconnected
                headingNoHmd.visible = !hmdconnected
            }
            disconnectSource(sourceName)
        }
    }
    
    Timer {
        id: timer;
        interval: 2000;
        onTriggered: {
            execHmdCheck.connectSource('qdbus org.kde.KWin /XR org.kde.kwin.XR.hmdconnected');
            execQuery.connectSource('qdbus org.kde.KWin /XR org.kde.kwin.XR.active');
        }
        repeat: true;
        triggeredOnStart:true;
        running: true;
    }
}


